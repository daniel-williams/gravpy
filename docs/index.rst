.. gravpy documentation master file, created by
   sphinx-quickstart on Tue Jul  9 22:26:36 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to gravpy's documentation!
======================================

Gravpy is a sandbox for gravitational wave astrophysics: it's not
intended to be used for heavy-weight gravitational wave data analysis,
and it started life as a series of iPython notebooks written up while
I was reading through papers when I was starting out in my PhD, but
it's started to snowball.

Gravpy is currently capable of plotting sensitivity curves for
ground-based interferometers, and for pulsar timing arrays, and their
antenna responses. It's also capable of calculating the spectra of CBC
merger events, and making plots of those.

It's early days and there are lots of other things to be done with the package!

User Guide
==========
.. toctree::
   :maxdepth: 2

   readme
   installation
   sources
   interferometers
   example
   usage
   other-software


Citing Gravpy
=============

.. image:: https://zenodo.org/badge/65474270.svg
   :target: https://zenodo.org/badge/latestdoi/65474270

If you use gravpy for a publication please consider citing it.
The code is archived on Zenodo, and has a DOI: 10.5281/zenodo.3938485.

The ``bibtex`` entry for `gravpy` is

::
   
   @software{gravpy,
   author       = {Daniel Williams},
   title        = {transientlunatic/gravpy},
   month        = jul,
   year         = 2020,
   publisher    = {Zenodo},
   version      = {v0.2.4},
   doi          = {10.5281/zenodo.3938484},
   url          = {https://doi.org/10.5281/zenodo.3938484}
   }
   

Developer Guide
===============

If you'd like to be involved in developing or expanding `gravpy` then you should start by reading the contribution guide.

.. toctree::
   :maxdepth: 2
	   
   contributing
   authors
   modules
   history

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

